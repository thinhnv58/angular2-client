var express = require('express');
var path = require('path');

var app = express();
var http = require('http');
var server = http.Server(app);
var io = require('socket.io')(server);
var querystring = require('querystring');


app.get('/', function(req, res, next){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('send message', function(msg){
    console.log(msg);
    var request = require("request");
    request({
      uri: "http://localhost:8000/comments/",
      method: "POST",
      form: {
        text: msg
      }
    }, function(error, response, body) {
      console.log(body);
    });



  })
});


app.use(express.static(path.join(__dirname, 'public')));
app.use('/node_modules', express.static(__dirname + '/node_modules'));





module.exports = {app: app, server: server};