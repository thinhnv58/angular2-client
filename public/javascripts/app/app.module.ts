import './rxjs-extensions';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppComponent }   from './app.component';
import { ContactSearchComponent } from './contact-search.component';
import { ContactSearchService } from './contact-search.service';
@NgModule({
  imports:      [ BrowserModule, HttpModule ],
  declarations: [ AppComponent, ContactSearchComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
