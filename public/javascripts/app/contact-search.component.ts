import { Component, OnInit } from '@angular/core';
import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';
import { ContactSearchService } from './contact-search.service';
import { Contact } from './contact';
@Component({
  selector: 'contact-search',
  template: `
  <div id="search-component">
    <h4>Contact  Search</h4>
    <input #searchBox id="search-box" (keyup)="search(searchBox.value)" />
    <div>
      <div *ngFor="let contact of contacts | async"
      (click)="logName(contact)"
      >
        {{ contact.name }}
        {{ contact.id }}
        {{ contact.address }}
      </div>
    </div>
  </div>


  `,    
  providers: [ContactSearchService]
})
export class ContactSearchComponent implements OnInit {
  contacts: Observable<Contact[]>;
  private searchTerms = new Subject<string>();
  constructor(
    private contactSearchService: ContactSearchService
  ){}
  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  logName(contact):void{
    console.log(contact.name)
  }
  ngOnInit(): void {
    this.contacts = this.searchTerms
      .debounceTime(300)        // wait for 300ms pause in events
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time
        // return the http search observable
        ? this.contactSearchService.search(term)
        // or the observable of empty contacts if no search term
        : Observable.of<Contact[]>([]))
      .catch(error => {
        // TODO: real error handling
        console.log(error);
        return Observable.of<Contact[]>([]);
      });
  }

}
