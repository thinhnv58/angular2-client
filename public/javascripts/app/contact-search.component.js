"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Observable_1 = require('rxjs/Observable');
var Subject_1 = require('rxjs/Subject');
var contact_search_service_1 = require('./contact-search.service');
var ContactSearchComponent = (function () {
    function ContactSearchComponent(contactSearchService) {
        this.contactSearchService = contactSearchService;
        this.searchTerms = new Subject_1.Subject();
    }
    // Push a search term into the observable stream.
    ContactSearchComponent.prototype.search = function (term) {
        this.searchTerms.next(term);
    };
    ContactSearchComponent.prototype.logName = function (contact) {
        console.log(contact.name);
    };
    ContactSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.contacts = this.searchTerms
            .debounceTime(300) // wait for 300ms pause in events
            .distinctUntilChanged() // ignore if next search term is same as previous
            .switchMap(function (term) { return term // switch to new observable each time
            ? _this.contactSearchService.search(term)
            : Observable_1.Observable.of([]); })
            .catch(function (error) {
            // TODO: real error handling
            console.log(error);
            return Observable_1.Observable.of([]);
        });
    };
    ContactSearchComponent = __decorate([
        core_1.Component({
            selector: 'contact-search',
            template: "\n  <div id=\"search-component\">\n    <h4>Contact  Search</h4>\n    <input #searchBox id=\"search-box\" (keyup)=\"search(searchBox.value)\" />\n    <div>\n      <div *ngFor=\"let contact of contacts | async\"\n      (click)=\"logName(contact)\"\n      >\n        {{ contact.name }}\n        {{ contact.id }}\n        {{ contact.address }}\n      </div>\n    </div>\n  </div>\n\n\n  ",
            providers: [contact_search_service_1.ContactSearchService]
        }), 
        __metadata('design:paramtypes', [contact_search_service_1.ContactSearchService])
    ], ContactSearchComponent);
    return ContactSearchComponent;
}());
exports.ContactSearchComponent = ContactSearchComponent;
//# sourceMappingURL=contact-search.component.js.map